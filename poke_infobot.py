import os
import pandas as pd
import sys

import utils.games as Games
import utils.html as uh
import utils.misc as um



class PokeInfoBot:
  """
  TODO docs of everything
  Multi-purpose object to obtain several kinds of Pokémon-related information

  It can be used by calling its dispatch() member which receives a list of
  input string arguments, which are parsed automatically. Some of these
  arguments can be keywords (see list in the constructor). Depending on the
  given arguments and keywords, dispatch() then calls a different method to
  output appropriate information:
  * 'caps' keyword (or equivalent) and game/generation: level caps
  * 'moves' keyword (or equivalent), Pokémon name, and generation: ...
  """

  def __init__(self):
    self.keyword_dict = {
      'location': ('loc', 'location', 'locations'),
      'moves': ('move', 'moves', 'moveset'),
      'caps': ('cap', 'caps', 'level', 'levels'),
      'tutors': ('tutor', 'tutors', 'tutormove', 'tutormoves')
    }
    self.clear_buffer()


  @staticmethod
  def help():
    """
    Return a help string
    """
    return ("Usage examples:\n"
            "6 poison\n"
            "drilbur 5\n"
            "excadrill loc white\n"
            "3 nidoking move\n"
            "levels dp\n"
            "icebeam 6\n"
            "hgss tutor\n"
            "tutor black2 ironhead\n"
            "growlithe flamethrower 5\n",
            "masterball 2")


  def _parse_args_and_initialize(self, input_args_list):
    """
    Parse argument into a dict and initialize relevant objects

    Also sets the self.gen member. Raises error if too few arguments

    Parameters
    ----------
    input_args_list: list of strings
      input arguments

    Returns
    -------
    args_dict: dict string -> string
      dict mapping a type of argument (e.g. 'game') to its value (e.g. 'black')
    """
    args_list = [_.lower() for _ in input_args_list]
    args_dict = dict.fromkeys(('games', 'gen', 'item', 'keyword', 'move',
                               'pokemon', 'tm', 'type'), None)

    # Find generation, and game(s) if any
    for arg in args_list:
      if args_dict['gen']:
        break
      games_from_arg = Games.get_list_of_games(arg)
      if len(games_from_arg) > 0:
        args_dict['gen'] = games_from_arg[0].gen

    if not args_dict['gen']:
      raise um.MessageError("You must provide the generation number or "
                               "game name(s)")
    self.gen = args_dict['gen']
    args_dict['games'] = games_from_arg

    # Read databases
    self._read_table_chart()
    self._read_poke_database()
    self._read_moves_database()

    # Parse arguments
    for arg in args_list:
      # Find Pokémon name
      poke = self.find_poke_by_name(arg)
      if poke is not None:
        args_dict['pokemon'] = poke['pokemon_name']

      # Find type
      if arg.title() in self.types:
        args_dict['type'] = arg.title()

      # Find item
      item = self.find_item_by_name(arg)
      if item is not None:
        args_dict['item'] = item['name']

      # Find move (also handle Psychic-the-type vs Psychic-the-move)
      argmove = 'psychic' if um.compare(arg, 'psychicmove') else arg
      move = self.find_move_by_name(argmove)
      if move is not None:
        args_dict['move'] = move['move_name']

      # Find TM/HM/TR
      for prefix in ('tm', 'hm', 'tr'):
        if arg.startswith(prefix):
          suffix = arg.partition(prefix)[2]
          if suffix.isdigit():
            args_dict['tm'] = arg
            break
      # Look for keywords
      for key, lis in self.keyword_dict.items():
        if arg in lis:
          args_dict['keyword'] = key

    return args_dict


  def print(self, *args, flush=False):
    """
    Store the given args list in string form, to be printed later

    If args is empty or flush is True, the print buffer will be displayed
    immediately and then cleared
    """
    for arg in args:
      self.print_buffer += str(arg)

    if len(args) == 0 or flush:
      print(self.print_buffer)
      self.clear_buffer()


  def get_buffer(self):
    """
    Return the text currently stored in the print buffer
    """
    return self.print_buffer


  def clear_buffer(self):
    """
    Clear the text currently stored in the print buffer
    """
    self.print_buffer = ""


  def _read_table_chart(self):
    """
    Read the type chart from file and store it into the class
    """
    chart_file = os.path.join('resources',
        'table_gen5minus.csv' if self.gen <= 5 else 'table_gen6plus.csv')
    self.chart = pd.read_csv(chart_file)
    # Store list of types
    self.types = self.chart.columns
    self.chart.set_index(self.types, inplace=True)


  def _read_poke_database(self):
    """
    Read the Pokémon database from file and store it into the class
    """
    pokes_file = os.path.join('resources',
        'pokemon_gen5minus.csv' if self.gen <= 5 else 'pokemon_gen6plus.csv')
    self.pokes_df = pd.read_csv(pokes_file, index_col='id')


  def find_poke_by_name(self, poke_name):
    """
    Find a Pokémon name in the database, or return None if not found
    """
    for _, row in self.pokes_df.iterrows():
      if um.compare(poke_name, row['pokemon_name']):
        return row
    return None


  def _read_moves_database(self):
    """
    Read the moves database from file and store it into the class
    """
    moves_file = os.path.join('resources',
        'moves_gen5minus.csv' if self.gen <= 5 else 'moves_gen6plus.csv')
    self.moves_df = pd.read_csv(moves_file, index_col='id')


  def find_move_by_name(self, move_name):
    """
    Find a Pokémon move name in the database, or return None if not found
    """
    for _, row in self.moves_df.iterrows():
      if um.compare(move_name, row['move_name']):
        return row
    return None


  def find_item_by_name(self, item_name):
    """
    Find a Pokémon move name in the database, or return None if not found
    """
    items_file = os.path.join('resources', 'items.csv')
    items_df = pd.read_csv(items_file)
    for _, row in items_df.iterrows():
      if um.compare(item_name, row['name']) and self.gen >= row['gen']:
        return row
    return None


  def display_level_caps(self, input_games):
    """
    Print the level caps for the given games

    'Level cap' means the highest Pokémon level in the party of each major
    Trainer in the games, such as Gym Leaders and League members
    """
    # Open level cap dataset from file
    level_caps = pd.read_csv(os.path.join('resources', 'level_caps.csv'))
    bw2_challenge = False
    # Loop over dataset rows i.e. games
    for _, row in level_caps.iterrows():
      curr_games = Games.get_list_of_games(row['games'], '/')
      if Games.lists_share_game(input_games, curr_games):
        self.print(f"Level caps for {row['games']}:\n")
        game_caps = [row['cap'+str(i)] for i in range(1, 9)]
        # Also display additional level caps for Alola games
        if 'Moon' in row['games']:
          game_caps.extend(row['other_caps'].split('/'))
        # Handle Challenge Mode level for BW2
        if 'Black 2' in row['games']:
          if not bw2_challenge:
            bw2_challenge = True
            self.print("(Normal Mode)\n")
          else:
            self.print("(Challenge Mode)\n")
        # Loop over level caps of the game
        for i, cap in enumerate(game_caps):
          self.print(f"cap {i+1}: {cap}\n")
        self.print(f"Elite Four: {row['e4']}\n")
        self.print(f"Champion: {row['champion']}\n")
        self.print("\n")


  def _scrape_moveset(self, input_poke):
    """
    Internal method to get the moveset of the input Pokémon in self.gen
    """
    # Initialize return string
    ret_strg = ""
    poke = self.find_poke_by_name(input_poke)
    if poke is not None:
      poke_name = poke['pokemon_name']
      ret_strg += f"{poke_name}'s moveset\n\n"
      poke_name = um.correct_name_for_urls(poke_name)
      # Get web page for the Pokémon learnset
      url = (f'https://bulbapedia.bulbagarden.net/wiki/{poke_name}_'
              '(Pok%C3%A9mon)/Generation_'
             f'{Games.get_gen_entry(self.gen, "roman")}_learnset')

      # Navigate BeautifulSoup object to extract needed information
      tables = uh.get_soup_from_url(url).find_all('table', class_='roundy')

      # Get table(s) of level-up moves
      curr_tables = uh.find_tables_by_href(tables, 'By_leveling_up')
      start_row = 6 if self.gen in (6, 7) else 5
      for table in curr_tables:
        ret_strg += uh.find_table_headers(table)
        lvlup_columns = None
        # Loop over moves
        for move_row in uh.get_table_rows(table, 6):
          # First tag = first level; remove redundant span tag if present
          if move_row[0].find('span') is not None:
             move_row[0].find('span').decompose()
          lvl = move_row[0].text.strip('\n')

          # Second tag: either second level or move name; remove redundant span
          # tag, but only in the former case
          span_tag = move_row[1].find('span')
          if span_tag is not None and \
                um.is_valid_level(span_tag.text.strip('\n')):
              move_row[1].find('span').decompose()
          lvl_2_maybe = move_row[1].text.strip('\n')

          if um.is_valid_level(lvl_2_maybe):
            # There are 2 columns of levels, for different games
            if lvlup_columns is None:
              lvlup_columns = Games.get_gen_entry(self.gen, 'levelup_columns')
              ret_strg += " / ".join(lvlup_columns) + "\n"
            ret_strg += f"{lvl} / {lvl_2_maybe} {move_row[2].text}"
          else:
            # There is only 1 column of levels
            ret_strg += f"{lvl} {move_row[1].span.text}\n"
        ret_strg += "\n"

      # Get table(s) for TM/HM/TR moves
      curr_tables = uh.find_tables_by_href(tables, 'By_TM')
      for table in curr_tables:
        ret_strg += uh.find_table_headers(table)
        # Loop over moves
        for move_row in uh.get_table_rows(table, start_row):
          if not move_row:
            ret_strg += "none\n"
            break
          move_num = uh.get_cleaned_tag_text(move_row[1])
          move_name = uh.get_cleaned_tag_text(move_row[2])
          ret_strg += f"{move_num} {move_name}\n"
        ret_strg += "\n"

      # Get table(s) for tutor moves, if any
      curr_tables = uh.find_tables_by_href(tables, 'By_tutoring')
      for table in curr_tables:
        if table is not None:
          ret_strg += uh.find_table_headers(table)
          # Loop over moves
          for move_row in uh.get_table_rows(table, start_row):
            if not move_row:
              ret_strg += "none\n"
              break
            ret_strg += move_row[0].text
        ret_strg += "\n"

      # Get table(s) for moves, if any, learnt by a previous evolution
      curr_tables = uh.find_tables_by_href(tables, 'By_a_prior_evolution')
      for table in curr_tables:
        ret_strg += uh.find_table_headers(table)
        # Loop over moves
        for move_row in uh.get_table_rows(table, start_row):
          if not move_row:
            ret_strg += "none\n"
            break
          ret_strg += move_row[1].text
        ret_strg += "\n"

    return ret_strg


  def display_moveset(self, input_poke):
    """
    Interface method to print output of _scrape_moveset()
    """
    self.print(self._scrape_moveset(input_poke))


  def display_move_in_moveset(self, input_poke, input_move):
    """
    Print input_move learn details if present in input_poke's moveset
    """
    moveset_text = self._scrape_moveset(input_poke)
    moveset_text = moveset_text.partition("\n")[2]
    if "\n" not in moveset_text:
      return

    self.print(f"Learning methods of {input_move} for {input_poke}\n")
    found = False
    title_buffer = ""
    for block in moveset_text.split("\n\n"):
      for line in block.split("\n"):
        if ":" in line:
          title_buffer += line + "\n"
        elif um.contained(input_move, line):
          self.print(title_buffer + line + "\n")
          title_buffer = ""
          found = True

    if not found:
      self.print("(does not learn it)\n")


  def display_poke(self, poke_name):
    """
    Print Pokémon basic information starting from poke_name

    This includes Pokédex number, typing, stats, abilities, and evolution
    method (if any)
    """
    # Loop over dataset lines i.e. Pokémon
    for idx, poke in self.pokes_df.iterrows():
      if um.compare(poke_name, poke['pokemon_name']) and \
          self.gen >= poke['gen']:
        self.print(f"#{idx[:3]} {poke['pokemon_name']}\n"
                   f"Typing: {poke['typing']}\n"
                   f"Base stats: {poke['stats']}\n")
        # Display Abilities, if they exist in this gen
        if self.gen >= 3:
          self.print(f"Abilities: {poke['ability1']}")
          if not pd.isna(poke['ability2']):
            self.print(" / " + poke['ability2'])
          if self.gen >= 5 and not pd.isna(poke['abilityh']):
            self.print(" / " + poke['abilityh'] + "(H)")
          self.print("\n")
        # Display evolution method, if any
        if not pd.isna(poke['evolution']):
          self.print(f"Evolution method: {poke['evolution']}\n")
        return


  def display_type_info(self, input_type):
    """
    Print the offensive and defensive matchups of the given type
    """
    for typ in self.types:
      if um.compare(input_type, typ):
        self.print(typ + " type\n")

        self.print("Attacking:\n")
        att_sup_eff = um.find_types_by_eff(self.chart.loc[typ], 2)
        att_not_eff = um.find_types_by_eff(self.chart.loc[typ], 0.5)
        att_immune  = um.find_types_by_eff(self.chart.loc[typ], 0)
        self.print("Super eff: " + um.join_types(att_sup_eff) + "\n"
                 "Not very eff: " + um.join_types(att_not_eff) + "\n"
                 "Immune: " + um.join_types(att_immune) + "\n\n")

        self.print("Defending:\n")
        def_sup_eff = um.find_types_by_eff(self.chart[typ], 2)
        def_not_eff = um.find_types_by_eff(self.chart[typ], 0.5)
        def_immune  = um.find_types_by_eff(self.chart[typ], 0)
        self.print("Super eff: " + um.join_types(def_sup_eff) + "\n"
                   "Not very eff: " + um.join_types(def_not_eff) + "\n"
                   "Immune: " + um.join_types(def_immune) + "\n")
        return


  def display_move_info(self, input_move):
    """
    Print basic information for the given move
    """
    for _, move in self.moves_df.iterrows():
      if um.compare(move['move_name'], input_move) and \
          self.gen >= move['gen']:
        self.print(f"Move name: {move['move_name']} ")
        if self.gen >= 4:
          self.print(f"({move['type']}, {move['category']})\n")
        else:
          self.print(f"({move['type']})\n")
        self.print(f"{move['power']} power, {move['accuracy']} accuracy, "
                   f"{move['pp']} PP\n")
        return


  def display_poke_locations(self, input_poke, input_games):
    """
    Print the encounter locations for the given Pokémon and game
    """
    poke = self.find_poke_by_name(input_poke)
    if poke is None:
      return

    # Prepare Serebii URL
    num_url = self.pokes_df[
        self.pokes_df['pokemon_name'] == poke['pokemon_name']
    ].index[0][:3]
    gen_alias = Games.gen_num_to_alias(self.gen)
    gen_url = '-' + gen_alias if self.gen > 1 else gen_alias
    url = f'https://serebii.net/pokedex{gen_url}/location/{num_url}.shtml'
    # Go to URL and retrieve all appropriate tables
    tables_html = uh.get_soup_from_url(url).find_all('table',
                                                     class_='dextable')[1:]
    self.print(f"Wild locations for {poke['pokemon_name']}\n")
    self.print("(source: Serebii)\n")
    try:
      tables_pd = pd.read_html(str(tables_html))
    except:
      # No table found
      tables_pd = None

    if tables_pd:
      # Scrape from Serebii, looping over found tables
      for table_game in tables_pd:
        # Skip empty tables
        if table_game.shape[0] <= 1:
          continue
        # Prepare correctly formatted name of game
        curr_games = table_game.loc[0, 1].replace("Pokémon ", "")
        if Games.lists_share_game(input_games,
                                  Games.get_list_of_games(curr_games)):
          # Not 'empty': prepare data to display
          self.print(curr_games + ":\n")
          data = table_game.loc[2:].copy()
          data.columns = table_game.loc[1]  # header
          data['Rarity'] = data['Rarity'].apply(um._format_pct)
          # Sort data in descending encounter % order
          data.sort_values(by=['Rarity', 'Location'], inplace=True,
                           ascending=[False, True])
          # Loop over single locations
          for _, row in data.iterrows():
            rarity = row['Rarity']
            rarity = int(rarity) if isinstance(rarity, float) else rarity
            self.print(f"{row['Location']} ({rarity}%, {row['Method']})")
            if 'Time' in row:  # for Gen 2 and Alola games
              self.print(f", time: {row['Time']}")
            elif 'Season' in row:  # for Gen 5 games
              self.print(f", season: {row['Season']}")
            if 'Initial or SOS' in row and 'Grass Patch' in row:  # Alola games
              self.print(f"; {row['Initial or SOS']}, "
                         f"{row['Grass Patch']}")
            self.print("\n")
          self.print(15*"-" + "\n\n")

    # Scrape from Bulbapedia
    # Prepare URL
    url = (f'https://bulbapedia.bulbagarden.net/wiki/{poke["pokemon_name"]}_'
           r'(Pok%C3%A9mon)')
    # Navigate BeautifulSoup object to extract needed information
    table_big = uh.get_soup_from_url(url).find('span', id='Game_locations') \
                  .find_next('table', class_='roundy').tbody \
                  .find_all('tr', recursive=False)[1:]
    self.print("(source: Bulbapedia)\n")
    # Loop over sub-tables of single generations
    for table_gen in table_big:
      table = table_gen.find('tbody').find('tbody')
      # Loop over entries for (groups of) games in the current gen
      for tr in table.find_all('tr', recursive=False):
        displayed_names = [t.text.strip("\n ") for t in tr.find_all('th')]
        curr_games = um.flatten(
            [Games.get_list_of_games(g) for g in displayed_names]
        )
        if Games.lists_share_game(input_games, curr_games):
          # We print this (group of) games
          ## Add ; in each newline tag, for readability's sake
          for sm in tr.find_all('small'):
            if not sm.text.startswith("("):
              sm.string = f"({sm.text})"
          ## Place content of small-font tags into (), for readability
          for br in tr.find_all('br'):
            br.string = "; "
          location = tr.td.text.strip("\n ")
          ## Print information
          self.print(" / ".join(displayed_names) + ": " + location + "\n")


  def display_tm_info(self, tm_code, input_games):
    """
    Print basic information on a TM, HM, or TR in the given game(s)

    tm_code can be the code for any TM, HM, or TR, e.g. "TM23", "HM01", "TR54"
    """
    # Open tutor location dataset from file
    tm_df = pd.read_csv(os.path.join('resources', 'tms.csv'))
    self.print(f"TM/HM information\n")
    # Loop over dataset rows i.e. TMs (etc)
    for _, row in tm_df.iterrows():
      curr_games = Games.get_list_of_games(row['games'], '/')
      if um.compare(tm_code, row['tm']) and \
          Games.lists_share_game(input_games, curr_games):
        self.print(f"In " + row['games'] + ":\n")
        self.print(f"{row['tm']} {row['move']}\n")
        self.print(f"Found at {row['location']}")
        self.print("\n\n")


  def display_item_location(self, item_name, input_games):
    """
    Displays location of the given item in the indicated game(s)
    """
    # Prepare Bulbapedia URL
    url = 'https://bulbapedia.bulbagarden.net/wiki/' + item_name
    # Navigate BeautifulSoup object to extract needed information
    soup = uh.get_soup_from_url(url)
    table_html = soup.find('span', id="Acquisition") \
                     .find_next('table', class_='roundy') \
                     .find('table')
    self.print(f"Location of item {item_name}\n")
    for item_row in uh.get_table_rows(table_html, 1):
      games_groups = item_row[0].find_all('a')
      curr_games = um.flatten(
        [Games.get_list_of_games(g['title'], " and ") for g in games_groups]
      )
      if not Games.lists_share_game(curr_games, input_games):
        continue

      displayed_names = " / ".join([g.full_name for g in curr_games])
      self.print(f"{displayed_names}:\n")
      fin_meth = item_row[1].text
      if fin_meth == "\n":
        fin_meth = "(none)\n"
      rep_meth = item_row[2].text
      if rep_meth == "\n":
        rep_meth = "(none)\n"
      self.print(f"Finite: {fin_meth}")
      self.print(f"Repeated: {rep_meth}\n")


  def display_tutors_location(self, input_games):
    """
    Print locations for move tutors and Move Reminders for the given game(s)
    """
    # Open tutor location dataset from file
    tutor_locs = pd.read_csv(os.path.join('resources', 'tutors.csv'))
    displayed_names = ' / '.join([g.full_name for g in input_games])
    self.print(f"Tutor moves in {displayed_names}\n\n")
    # Loop over dataset rows i.e. tutors
    for _, row in tutor_locs.iterrows():
      curr_games = Games.get_list_of_games(row['games'], '/')
      if Games.lists_share_game(input_games, curr_games):
        self.print(f"Move tutor at {row['location']} for {row['currency']}:\n")
        self.print(row['moves'].replace("/", "\n"))
        self.print("\n\n")


  def display_move_tutor_location(self, input_move, input_games):
    """
    Display location of the tutor in 'games' teaching 'input_move', if any
    """
    # Open tutor location dataset from file
    tutor_locs = pd.read_csv(os.path.join('resources', 'tutors.csv'))
    displayed_names = ' / '.join([g.full_name for g in input_games])
    self.print(f"Tutor moves in {displayed_names}\n")
    # Loop over dataset rows i.e. tutors
    for _, row in tutor_locs.iterrows():
      curr_games = Games.get_list_of_games(row['games'], '/')
      if Games.lists_share_game(input_games, curr_games):
        for move in row['moves'].split('/'):
          if um.compare(input_move, move):
            self.print(f"Location for {move} tutor:\n")
            self.print(f"{row['location']} for {row['currency']}\n")


  def dispatch(self, inputs_list, flush=False):
    """
    Run all user interface methods of the class
    """
    if len(inputs_list) < 2:
      self.print(self.help())
      return

    args_dict = self._parse_args_and_initialize(inputs_list)

    # Consider all possible methods one by one
    if args_dict['keyword'] == 'caps':
      self.display_level_caps(args_dict['games'])

    if args_dict['tm']:
      self.display_tm_info(args_dict['tm'], args_dict['games'])

    if args_dict['type']:
      self.display_type_info(args_dict['type'])

    if args_dict['item']:
      self.display_item_location(args_dict['item'], args_dict['games'])

    if args_dict['keyword'] == 'tutors' and args_dict['move']:
      self.display_move_tutor_location(args_dict['move'], args_dict['games'])

    if args_dict['keyword'] == 'tutors' and not args_dict['move']:
      self.display_tutors_location(args_dict['games'])

    if args_dict['move'] and args_dict['keyword'] != 'tutors' \
                         and not args_dict['pokemon']:
      self.display_move_info(args_dict['move'])

    if args_dict['pokemon'] and args_dict['move']:
      self.display_move_in_moveset(args_dict['pokemon'], args_dict['move'])

    if args_dict['pokemon'] and not args_dict['move'] \
                            and not args_dict['keyword']:
      self.display_poke(args_dict['pokemon'])

    if args_dict['pokemon'] and args_dict['keyword'] == 'moves':
      self.display_moveset(args_dict['pokemon'])

    if args_dict['pokemon'] and args_dict['keyword'] == 'location':
      self.display_poke_locations(args_dict['pokemon'], args_dict['games'])

    if flush:
      self.print()



if __name__ == '__main__':
  bot = PokeInfoBot()
  bot.dispatch(sys.argv[1:], flush=True)
