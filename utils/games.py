from collections import namedtuple

import utils.misc as um


SingleGame = namedtuple('SingleGame', ['full_name', 'gen'])

_gens_dict = {
  1: {
      'roman': 'I',
      'games': ('Red', 'Blue', 'Yellow'),
      'groups': {
          'rb': ('Red', 'Blue'),
          'rby': ('Red', 'Blue', 'Yellow')
      },
      'levelup_columns': ('RB', 'Y')
  },

  2: {
      'roman': 'II',
      'games': ('Gold', 'Silver', 'Crystal'),
      'groups': {
          'gs': ('Gold', 'Silver'),
          'gsc': ('Gold', 'Silver', 'Crystal')
      },
      'levelup_columns': ('GS', 'C')
  },

  3: {
      'roman': 'III',
      'games': ('Ruby', 'Sapphire', 'FireRed', 'LeafGreen', 'Emerald'),
      'groups': {
          'rs': ('Ruby', 'Sapphire'),
          'rse': ('Ruby', 'Sapphire', 'Emerald'),
          'frlg': ('FireRed', 'LeafGreen')
      },
      'levelup_columns': ('RSE', 'FRLG')
  },

  4: {
      'roman': 'IV',
      'games': ('Diamond', 'Pearl', 'Platinum', 'HeartGold', 'SoulSilver'),
      'groups': {
          'dp': ('Diamond', 'Pearl'),
          'dpp': ('Diamond', 'Pearl', 'Platinum'),
          'dppt': ('Diamond', 'Pearl', 'Platinum'),
          'hgss': ('HeartGold', 'SoulSilver')
      },
      'levelup_columns': ('DP', 'PtHGSS')
  },

  5: {
      'roman': 'V',
      'games': ('Black', 'White', 'Black 2', 'White 2'),
      'groups': {
          'bw': ('Black', 'White'),
          'bw2': ('Black 2', 'White 2'),
          'b2w2': ('Black 2', 'White 2')
      },
      'levelup_columns': ('BW', 'B2W2')
  },

  6: {
      'roman': 'VI',
      'games': ('X', 'Y', 'Omega Ruby', 'Alpha Sapphire'),
      'groups': {
          'xy': ('X', 'Y'),
          'oras': ('Omega Ruby', 'Alpha Sapphire')
      },
      'levelup_columns': ('XY', 'ORAS')
  },

  7: {
      'roman': 'VII',
      'games': ('Sun', 'Moon', 'Ultra Sun', 'Ultra Moon',
                "Let's Go Pikachu", "Let's Go Eevee"),
      'groups': {
          'sm': ('Sun', 'Moon'),
          'usm': ('Ultra Sun', 'Ultra Moon'),
          'usum': ('Ultra Sun', 'Ultra Moon'),
          'lgpe': ("Let's Go Pikachu", "Let's Go Eevee"),
          'letsgo': ("Let's Go Pikachu", "Let's Go Eevee"),
          'lgp': ("Let's Go Pikachu", ),
          'lge': ("Let's Go Eevee", ),
      },
      'levelup_columns': ('SM', 'USUM')
  },

  8: {
      'roman': 'VIII',
      'games': ('Sword', 'Shield', 'Brilliant Diamond', 'Shining Pearl',
                'Legends: Arceus'),
      'groups': {
          'swsh': ('Sword', 'Shield'),
          'bdsp': ('Brilliant Diamond', 'Shining Pearl'),
          'la': ('Legends: Arceus', )
      }
  }

}


def get_gen_entry(gen, key):
  """
  Return characteristic 'key' of gen 'gen', from the _gens_dict object

  Raises KeyError if either gen or key are not valid
  """
  try:
    return _gens_dict[int(gen)][key]
  except:
    raise KeyError(f"Cannot get '{key}' for gen '{gen}'")


def gen_roman_to_num(roman):
  """
  Return the integer number representation of a generation Roman number

  Raises ValueError if the input is not a valid generation Roman number
  """
  for gen_num, gen_dic in _gens_dict.items():
    if um.compare(roman, gen_dic['roman']):
      return gen_num

  raise ValueError(f"Generation roman number '{roman}' not found")


def gen_num_to_alias(num):
  """
  Return the alias a of generation integer number

  The alias is the string with the initials of the first/main games of the
  generation. Raises ValueError if the input is not a valid generation integer
  number
  """
  try:
    return list(_gens_dict[int(num)]['groups'].keys())[0]
  except:
    raise ValueError(f"Generation number '{num}' not found")


def _get_game(input_game):
  """
  Return SingleGame namedtuple based on input game name (None if not found)

  This is an internal method, to be used in the get_list_of_games() interface
  function
  """
  for gen_num, gen_dic in _gens_dict.items():
    for game in gen_dic['games']:
      if um.compare(input_game, game):
        return SingleGame(full_name=game, gen=gen_num)

  return None


def _get_group(input_group):
  """
  Return list of SingleGame namedtuples based on the input group name

  This is an internal method, to be used in the get_list_of_games() interface
  function
  """
  for gen_num, gen_dic in _gens_dict.items():
    for group, games in gen_dic['groups'].items():
        if um.compare(input_group, group):
          return [SingleGame(full_name=g, gen=gen_num) for g in games]

  return []


def _get_games_of_gen(input_gen):
  """
  Return list of SingleGame namedtuples corresponding to the input gen

  This is an internal method, to be used in the get_list_of_games() interface
  function
  """
  if input_gen in _gens_dict:
    return [SingleGame(full_name=g, gen=input_gen)
              for g in _gens_dict[input_gen]['games']
           ]

  return []


def get_list_of_games(input_val, separator=None):
  """
  Return list of SingleGame namedtuples based on the input string

  This function attempts to interpret input_val as a generation number, group
  of games (alias), or game name. If separator is provided, input_val will be
  assumed to be a string and splitted by that value, and games will be
  initialized with the resulting splitted list, after removing some title words
  """
  if separator is not None:
    title = input_val
    for strg in (" Versions", " Version", "Pokémon ", "Pokémon: "):
      title = title.replace(strg, "")
    if title == "Black and White 2":
      title = "Black 2 and White 2"
    # Call this function recursively
    nested_lists = [get_list_of_games(g, None) for g in title.split(separator)]
    return um.flatten(nested_lists)

  # Try with generations
  if input_val in _gens_dict:
    return _get_games_of_gen(input_val)
  if input_val.isdigit() and int(input_val) in _gens_dict:
    return _get_games_of_gen(int(input_val))

  # Try with groups of games
  games_list = _get_group(input_val)
  if len(games_list) > 0:
    return games_list

  # Try with individual games
  game = _get_game(input_val)
  if game is not None:
    return [game]

  return []


def lists_share_game(list1, list2):
  """
  Find whether two given list of SingleGames objects have a common game or not

  Parameters
  ----------
  list1, list2: list of SingleGames objects
    They each represent a list of games. True will be returned if there's at
    least one common game among them, otherwise False will be returned
  """
  for game1 in list1:
    for game2 in list2:
      if game1 == game2:
        return True

  return False
