from collections import namedtuple
from itertools import chain


_type_to_emoji = {
  'Normal':   '⚪', 'Fighting': '💪', 'Flying':   '🕊',
  'Poison':   '☠',  'Ground':   '⛰', 'Rock':     '🪨',
  'Bug':      '🐛', 'Ghost':    '👻', 'Steel':    '⚙',
  'Fire':     '🔥', 'Water':    '💧', 'Grass':    '🌿',
  'Electric': '⚡', 'Psychic':  '🧠', 'Ice':      '❄',
  'Dragon':   '🐲', 'Dark':     '🕶', 'Fairy':    '🧚'
}


class MessageError(Exception):
  """
  A kind of error whose content is to be forwarded to the user
  """
  pass


def flatten(list_of_lists):
  """
  Return joined list from the given nested list of lists
  """
  return list(chain.from_iterable(list_of_lists))


def find_types_by_eff(series, eff):
  """
  Find Pokémon types whose effectiveness coefficient is equal to eff
  """
  return series.loc[lambda x : x == eff].index


def join_types(lis):
  """
  Join Pokémon type strings in lis, or return -- if empty
  """
  if lis.empty:
    return "--"
  return ", ".join(lis)


def compare(str1, str2):
  """
  Compare two strings, ignoring case and all non-alphanumeric characters
  """
  if str1 is None or str2 is None:
    return False

  str1_comp = ''.join([c for c in str1.lower() if c.isalnum()])
  str2_comp = ''.join([c for c in str2.lower() if c.isalnum()])
  return str1_comp == str2_comp


def contained(str1, str2):
  """
  Check if str1 is included in str2 without taking spaces or case into account
  """
  return str1.lower().replace(" ", "") in str2.lower().replace(" ", "")


def _format_pct(strg):
  """
  Function used internally to display rarity in location DataFrame tables
  """
  ret = strg.replace('%', '').replace(' ', '')
  try:
    return float(ret)
  except:
    return ret


def is_valid_level(strg):
  """
  Return whether or not the given string is a valid level-up learnset level
  """
  return strg.isdigit() or strg in ('N/A', 'Evo.')


def correct_name_for_urls(poke_name):
  """
  Return corrected Pokémon name so that it can correctly be inserted in URLs
  """
  # Handle Nidorans names, which have weird symbols in them
  if compare(poke_name, 'Nidoran-M'):
    return 'Nidoran♂'
  if compare(poke_name, 'Nidoran-F'):
    return 'Nidoran♀'

  if '-' in poke_name:
    # Handle alternate forms, except actual names with '-' in them, i.e. Ho-Oh,
    # Porygon-Z, and the Kommo-o family
    hyphen_split = poke_name.split('-')
    for suffix in ('oh', 'z', 'o'):
      if compare(hyphen_split[1], suffix):
        # return full name
        return poke_name
    # alternate form: return name without suffix
    return hyphen_split[0]

  return poke_name
