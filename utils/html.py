from bs4 import BeautifulSoup
from requests_html import HTMLSession


def get_soup_from_url(url):
  """
  Perform an HTTP request with url and return a new BeautifulSoup object
  """
  session = HTMLSession()
  html = session.get(url).text
  return BeautifulSoup(html, features='lxml')


def find_tables_by_href(table_list, href):
  """
  Find all tables in table_list, if any, whose 'a' tag contains href
  """
  ret_tables = []
  for table in table_list:
    tag = table.find('a')
    if 'transparent' not in table['style'] and tag is not None \
                                           and href in tag['href']:
      ret_tables.append(table)

  return ret_tables


def find_table_headers(table):
  """
  Return the text of the headers of the given table
  """
  ret_strg = ""
  tag_types = ('h4', 'h5', 'h6')
  for typ in tag_types:
    tag = table.find_previous(typ)
    if tag:
      ret_strg += tag.text + ":\n"

  return ret_strg


def get_table_rows(table, start):
  """
  Generator to loop over rows of the given table

  Parameters
  ----------
  table: BeautifulSoup object
    moveset table

  start: integer
    number of rows to skip at the beginning of the table
  """
  for move_tag in table.find_all('tr')[start:-1]:
    yield move_tag.find_all('td')


def get_cleaned_tag_text(tag):
  """
  Return text of tag cleaned of spaces/newlines, including sup subtags
  """
  sup_tag = tag.find('sup')
  if sup_tag:
    sup_text = sup_tag.text.replace("\n", "").replace(" ", "")
    sup_tag.decompose()
    tag.string = tag.text.replace("\n", "").strip(" ") + f"({sup_text})"

  return tag.text.replace("\n", "").strip(" ")
