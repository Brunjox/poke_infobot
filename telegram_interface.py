from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, \
                         CallbackContext

import poke_infobot
from utils.misc import MessageError


def bot_help(update: Update, context: CallbackContext) -> None:
  """
  Display help of PokeInfoBot class
  """
  msg = poke_infobot.PokeInfoBot.help()
  context.bot.send_message(update.message.chat_id, msg)


def answer(update: Update, context: CallbackContext) -> None:
  """
  Take input message from user and run PokeInfoBot to obtain the output
  """
  print(f"{update.message.from_user.first_name} "
       f"({update.message.from_user.id}) wrote: {update.message.text}")

  if update.message.text:
    args = update.message.text.strip("\n").split()
    try:
      infobot = poke_infobot.PokeInfoBot()
      infobot.dispatch(args, flush=False)
      msg = infobot.get_buffer()
      assert msg != ""
    except AssertionError:
      print("Warning: empty response text")
      msg = "(no info)"
    except MessageError as me:
      print("Warning: dispatch() raised a MessageError:", me)
      msg = "Error :S " + str(me)
    except Exception as e:
      print("Warning: dispatch() raised a generic error:")
      print(type(e), "-", e)
      msg = "Error :S"

    context.bot.send_message(update.message.chat_id, msg)

    print("Answer sent successfully\n")


def main() -> None:
  with open('token.txt', 'r') as f:
    updater = Updater(f.read())

  # Get the dispatcher to register handlers. Then, we register each handler
  # and the conditions the update must meet to trigger it
  dispatcher = updater.dispatcher

  # answer any message that is not a command
  dispatcher.add_handler(MessageHandler(~Filters.command, answer))
  dispatcher.add_handler(CommandHandler('start', bot_help))
  dispatcher.add_handler(CommandHandler('help', bot_help))

  # Start the Bot
  updater.start_polling()

  # Run the bot until you press Ctrl-C
  updater.idle()



if __name__ == '__main__':
    main()
