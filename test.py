import poke_infobot


args_strings = """
6 poison
drilbur 5
excadrill loc white
3 nidoking move
nidoking move 6
move ninetales 7
7 cap
levels dp
emerald psychicmove
icebeam 6
hgss tutor
tutor black
oras tutor covet
growlithe flamethrower 5
growlithe surf 5
tm26 4
masterball 1
""".strip("\n")


def test_arg_parse():
  args_list = "tapulele usum rock wake-upslap loc tm13".split()
  print("Test argument parsing")
  print("input:", *args_list)
  bot = poke_infobot.PokeInfoBot()
  args_dict = bot._parse_args_and_initialize(args_list)
  print(args_dict)
  print("gen =", bot.gen, "\n")


def run_tests():
  for i, args in enumerate(args_strings.split("\n")):
    print(70*"#")
    print(f"{i}) Running PokeInfoBot with '{args}'")
    bot = poke_infobot.PokeInfoBot()
    bot.dispatch(args.split())
    assert bot.print_buffer != ""
    bot.print(flush=True)



if __name__ == '__main__':
  test_arg_parse()
  run_tests()
